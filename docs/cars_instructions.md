# Initialize DB

## 1. Create model

```python
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Car(models.Model):

    ENGINE_BENZIN = 'b'
    ENGINE_DIESEL = 'd'

    ENGINE_CHOICES = [
        (ENGINE_BENZIN, _('Benzin')),
        (ENGINE_DIESEL, _('Diesel')),
    ]

    title = models.CharField(max_length=140)
    link = models.URLField(verbose_name="Advertisement Link")
    company = models.CharField(max_length=140)
    year = models.IntegerField()
    engine_type = models.CharField(max_length=1, choices=ENGINE_CHOICES)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title', )
```

## 2. Update DB
```shell
$ python manage.py makemigrations
Migrations for 'cars':
  cars/migrations/0001_initial.py:
    - Create model Car

```
2. Migrate
```shell
$ python manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, cars, contenttypes, sessions
Running migrations:
  Applying cars.0001_initial... OK
```



## 3. Admin interface

1. edit cars/admin.py
```python
from django.contrib import admin

import models


class CarAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_filter = ['engine_type']
    search_fields = ['title', 'company']


admin.site.register(models.Car, CarAdmin)
```

2. create admin (super)user
```shell
$ python manage.py createsuperuser
Username (leave blank to use 'igavran'): admin
Email address:                   
Password: 
Password (again): 
Superuser created successfully.
```

3. goto http://localhost/admin

# Display data

## 1. Implement read-only views

```python
from django.views.generic import View

class CarListView(View):
    """
    View for displaying list of cars.
    """

    def get(self, request, *args, **kwargs):
        cars = Car.objects.all()
        context = {
            'cars': cars
        }
        return render(request, 'cars/car_list.html', context=context)


class CarDetailsView(View):
    """
    View for displaying car details.
    """

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            car = Car.objects.get(id=kwargs['car_id'])
            context['car'] = car
        except Car.DoesNotExist:
            messages.error(request,
                           'No car with id: {}'.format(kwargs['car_id']))
        return render(request, 'cars/car_details.html', context=context)

```
## 1. Implement edit views

### 1. Forms
[Docs](https://docs.djangoproject.com/en/1.10/topics/forms/)

cars/forms.py
```python
from django import forms

from cars import models

class CarEditForm(forms.Form):
    # id = forms.IntegerField(widget=forms.HiddenInput())
    title = forms.CharField(label='Title', max_length=255)
    link = forms.URLField(label='External link', max_length=255)
    company = forms.CharField(label='Company', max_length=255)
    year = forms.IntegerField(label='Year')
    engine_type = forms.ChoiceField(label="Engine Type", choices=models.Car.ENGINE_CHOICES)
    

```

cars/views.py
```python
class CarEditView(View):
    """
    View for editing car details.
    """

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            car = Car.objects.get(id=kwargs['car_id'])
            context['car'] = car
        except Car.DoesNotExist:
            messages.error(request,
                           'No car with id: {}'.format(kwargs['car_id']))

        context['car_form'] = CarEditForm(initial={
            'id': car.id,
            'title': car.title,
            'link': car.link,
            'company': car.company,
            'year': car.year,
            'engine_type': car.engine_type
        })
        return render(request, 'cars/car_edit.html', context=context)

    def post(self, request, *args, **kwargs):
        car_form = CarEditForm(request.POST)
        car = Car.objects.get(id=kwargs['car_id'])
            
        if car_form.is_valid():
            car.title = car_form.cleaned_data['title']
            car.link = car_form.cleaned_data['link']
            car.company = car_form.cleaned_data['company']
            car.year = car_form.cleaned_data['year']
            car.engine_type = car_form.cleaned_data['engine_type']
            car.save()
            messages.success(request, 'Object saved!')
            return HttpResponseRedirect(reverse('car_details', kwargs={
                'car_id': car.id
            }))
        return render(request, 'cars/car_edit.html', context={
            'car': car,
            'car_form': car_form
        })
```

templates/cars/car_edit.py
```html

<h3>Edit car: {{ car.title}}</h3>
<form novalidate action="{% url 'car_edit' car_id=car.id %}" method="post">
    {% csrf_token %}
    {{ car_form }}
    <div class="button-group">
        <input class="button primary" type="submit" value="Save">
        <a class="button secondary" href="{% url 'car_details' car_id=car.id %}">Cancel</a>
    </div>
</form>

```

### TODO: Implement car delete view

### Model Forms

[Docs](https://docs.djangoproject.com/en/1.10/topics/forms/modelforms/)


cars/forms.py
```python
class CarModelForm(forms.ModelForm):
    class Meta:
        model = models.Car
        fields = ['title', 'link', 'company', 'year', 'engine_type']

```

cars/views.py
```python
class CarEditView(View):
    """
    View for editing car details.
    """

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            car = Car.objects.get(id=kwargs['car_id'])
            context['car'] = car
        except Car.DoesNotExist:
            messages.error(request,
                           'No car with id: {}'.format(kwargs['car_id']))

        context['car_form'] = CarModelForm(instance=car)
        return render(request, 'cars/car_edit.html', context=context)

    def post(self, request, *args, **kwargs):
        
        car = Car.objects.get(id=kwargs['car_id'])
        car_form = CarModelForm(request.POST, instance=car)
            
        if car_form.is_valid():
            car = car_form.save()
            messages.success(request, 'Object saved!')
            return HttpResponseRedirect(reverse('car_details', kwargs={
                'car_id': car.id
            }))
        return render(request, 'cars/car_edit.html', context={
            'car': car,
            'car_form': car_form
        })
```

### TODO: Implement "Add new car" 


# Django Authentication

## 1. Setup

- include django.cotrib.auth django app
- include middleware
- defined URLs
- implement templates
- auth template tags


# Django management commands

- implement django management command for importing CSV data

## List view improvements - Pagination

cars/views.py
```python
class CarListView(View):
    """
    View for displaying list of cars.
    """

    def get(self, request, *args, **kwargs):
        cars = Car.objects.all()
        paginator = Paginator(cars, 10)
        page = request.GET.get('page')
        try:
            cars = paginator.page(page)
        except PageNotAnInteger:
            cars = paginator.page(1)
        except EmptyPage:
            cars = paginator.page(paginator.num_pages)
        context = {
            'cars': cars,
            'new_car_form': CarModelForm()

        }
        return render(request, 'cars/car_list.html', context=context)
```

templates/cars/car_list.html
```html
<div class="row">
    <div class="small-12 column">
        <ul class="pagination text-center" role="navigation" aria-label="Pagination">
            {% if cars.has_previous %}
                <li class="pagination-previous"><a href="?page={{ cars.previous_page_number }}">Previous</a></li>
            {% else %}
                <li class="pagination-previous disabled">Previous</li>
            {% endif %}
            {% if cars.has_next %}
                <li class="pagination-next"><a href="?page={{ cars.next_page_number }}">Next</a></li>
            {% else %}
                <li class="pagination-next disabled">Next</li>
            {% endif %}
        </ul>
    </div>
</div>
```


## Error Handling

1. Improve error handling in car details view.

'''python
class CarDetailsView(View):
    """
    View for displaying car details.
    """

    def get(self, request, *args, **kwargs):
        context = {
            'car': get_object_or_404(Car, id=kwargs['car_id'])
        }
        return render(request, 'cars/car_details.html', context=context)
'''

2. Let's add custom 404-Not Found Page

Create 404.html in `root` templates folder.


# Django Models
[Docs](https://docs.djangoproject.com/en/1.10/topics/db/models/)

1. Experiment with objects in django shell
[Docs] (https://docs.djangoproject.com/en/1.10/topics/db/queries/)


2. Add filter car_list view.


