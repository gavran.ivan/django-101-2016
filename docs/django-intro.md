# Introduction to Django

Content:

1. How to install Django
2. Create first Django app
3. Django project structure
4. Main components of Django app

## Install Django


### Install the latest version

```console
$ pip install Django
```

### Install specific version. Following command will install Django version 1.9

```console
$ pip install Django==1.9
```

> **Tip**
> Use [`virtualenv`](https://virtualenv.pypa.io/en/stable/) for Django projects.
> - what is virtualenv?
> - how to install virtualenv?
> - how to use virtualenv?

### Verify Django installation

1. Print version info:
    ```console
    $ python -c "import django; print django.get_version()"

    1.10.0
    ```
2. Check if `django-admin` command is available
    ```console
    $ django-admin

    Type 'django-admin help <subcommand>' for help on a specific subcommand.

    Available subcommands:

    [django]
        check
        compilemessages
        createcachetable
        dbshell
        diffsettings
        dumpdata
        flush
        inspectdb
        loaddata
        makemessages
        makemigrations
        migrate
        runserver
        sendtestemail
        shell
        showmigrations
        sqlflush
        sqlmigrate
        sqlsequencereset
        squashmigrations
        startapp
        startproject
        test
        testserver
    Note that only Django core commands are listed as settings are not properly configured (error: Requested setting INSTALLED_APPS, but settings are not configured. You must either define the environment variable DJANGO_SETTINGS_MODULE or call settings.configure() before accessing settings.).
    ```

## Our First Django App

Django provides command `django-admin` for various administrative tasks, one of which is to create scafolding of a new Django project.

```bash
$ django-admin startproject usedcarswebsite
```

This will create new folder `usedcarswebsite` with following content:
```
usedcarswebsite/
├── manage.py
└── usedcarswebsite
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```

Let's see what was generated for us. Execute following command in terminal:
```console
$ cd usercarswebsite
$ python manage.py runserver
```


### `manage.py` - Command line app management

`manage.py` script provides number of administrative commands for managing django project. You can see list of all available subcommands by executing:
```console
$ python manage.py
```

> **note**: `manage.py` vs `django-admin`
>
> `manage.py` provides all functionalities as django-admin but also takes care of a few project specific things:
>
> * adds your project's path on **sys.path**
> * sets **DJANGO_SETTINGS_MODULE** environment variable so that it points to your project's **settings.py** file

Subset of the most frequently used subcommands:

* `runserver` - start development server
* `makemigrations` - create data migrations files. 
* `migrate` - apply migrations to database
* `startapp` - add new app to the project
* `createsuperuser` - create admin user for django project.


### Django apps

Let's add new application to `usedcarswebsite` Django project.
```console
$ python manage.py startapp cars
```
And here is how our project's folder structure looks like now:
```console
usedcarswebsite/
├── cars
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
├── db.sqlite3
├── manage.py
└── usedcarswebsite
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    ├── wsgi.py
```

> **note**: Django Project vs Django App
>
>Django project is built from one or more of Django apps. Project combines one or more Django apps into one *web application*. 
Project defines:
* execution environment settings for all apps (database, security, logging, etc.),
* top level URL routing,
* WSGI application

### Views

#### "Hello World" View

Let's implement index view for cars app by adding following code to `cars/views.py`:

```python
from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello world! This is cars app index site.")
```
The code above defines the simplest view possible in Django framework.

Next step is to bind index view to some URL. The simplest way to do this is to add an entry to `usercarswebsite/urls.py`:

```python
from django.conf.urls import url

from cars import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
]

```

The code above binds cars' index view to root of the django project.

*Todo*
Let's correct this and bind cars.views.index to http://localhost:8000/cars

### URL routing

[Docs](https://docs.djangoproject.com/en/1.10/topics/http/urls/)

Django provides all necessary tools for building flexible URL routing schemes.
The best practice is to define URL routing for each app and than include all app routing rules into project URL routing rules.

1. Create urls.py module in cars app (`cars/urls.py`) and add following code:
```Python
from django.conf.urls import url
from cars import views
urlpatterns = [
    url(r'^$', views.index)
]
```


2. Include cars app urlpatterns in project URL routing scheme by adding following code to `usedcarswebsite/urls.py`:
```python
from django.conf.urls import url, include
from django.contrib import admin

import cars.urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^cars/', include(cars.urls.urlpatterns, namespace='cars')),
]

```

> **Note**:
>
* url() function [Docs](https://docs.djangoproject.com/en/1.10/ref/urls/#django.conf.urls.url)
* include() function [Docs](https://docs.djangoproject.com/en/1.10/ref/urls/#django.conf.urls.include)

### Dive into views

#### View functions
A view function, or view for short, is simply a Python function that takes a Web request and returns a Web response. This response can be the HTML contents of a Web page, or a redirect, or a 404 error, or an XML document, or an image... or anything, really. [Docs](https://docs.djangoproject.com/en/1.10/topics/http/views/)

```Python

def hello(request):
    return HttpResponse("Hello world! This is cars app index site.")

def hello(request):
    return JsonResponse({'status': 'success'}, status_code=200)

def index(request):
    return render(request, 'index.html')

```

#### Class-based views

[Docs](https://docs.djangoproject.com/en/1.10/topics/class-based-views/)

[Ref docs](https://docs.djangoproject.com/en/1.10/ref/class-based-views/)

Class-based views provide an alternative way to implement views as Python objects instead of functions. They do not replace function-based views, but have certain differences and advantages when compared to function-based views:

Organization of code related to specific HTTP methods (GET, POST, etc.) can be addressed by separate methods instead of conditional branching.
Object oriented techniques such as mixins (multiple inheritance) can be used to factor code into reusable components.


A view is a callable which takes a request and returns a response.


**Tasks**

1. Redirect view. Implement "main index view" - index view for cars application which redirects user to cars/index view. 
2. Query params. Implement calculator view.
3. Error handling. 
4. Path params

