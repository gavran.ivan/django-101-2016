from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers

from cars.models import Car


class CarListView(View):
        
    def get(self, request, *args, **kwargs):
        cars = Car.objects.all()
        query = request.GET.get('q')
        if query:
            cars = cars.filter(title__contains=query)
        paginator = Paginator(cars, 10)
        page = request.GET.get('page')
        try:
            cars = paginator.page(page)
        except PageNotAnInteger:
            cars = paginator.page(1)
        except EmptyPage:
            cars = paginator.page(paginator.num_pages)
        
        result = {
            'items': [{'title': car.title, 'link': car.link} for car in cars],
            'previous_page': cars.previous_page_number() if cars.has_previous() else None,
            'next_page': cars.next_page_number()if cars.has_next() else None,
        }
        return JsonResponse(result)


class CarListSerializeView(View):
        
    def get(self, request, *args, **kwargs):
        cars = Car.objects.all()
        query = request.GET.get('q')
        if query:
            cars = cars.filter(title__contains=query)
        paginator = Paginator(cars, 10)
        page = request.GET.get('page')
        try:
            cars = paginator.page(page)
        except PageNotAnInteger:
            cars = paginator.page(1)
        except EmptyPage:
            cars = paginator.page(paginator.num_pages)
        
        serialized = serializers.serialize('json', cars)
        return HttpResponse(serialized, content_type='application/json')


class CarDetailsView(View):
    pass
