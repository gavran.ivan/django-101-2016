from django.conf.urls import url

from restsimple import views

urlpatterns = [
    url(r'^$', views.CarListView.as_view(), name='car_list'),
    url(r'^(?P<car_id>[\w]+)/$', views.CarDetailsView.as_view(), name='car_details'),
]
