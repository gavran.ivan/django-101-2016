from __future__ import absolute_import

import argparse
import logging
import csv

from django.core.management.base import BaseCommand

from cars.models import Car

logger = logging.getLogger('django')


class Command(BaseCommand):
    help = '''Import data from old db dump.
    Params:
        datadir: folder which MUST containt following files:
            - users.csv
            - samples.csv
            - basic_profiles.csv

    '''

    def add_arguments(self, parser):
        parser.add_argument('inputfile',
                            type=argparse.FileType('r'))
        parser.add_argument('--clear',
                            dest='clear',
                            action='store_true',
                            default=False)

    def handle(self, *args, **kwargs):
        file = kwargs['inputfile']
        dialect = csv.Sniffer().sniff(file.read(1024), delimiters=";,\t")
        print 'Delimiter: {}'.format(dialect.delimiter)
        print 'Quotechar: {}'.format(dialect.quotechar)

        file.seek(0)

        csvreader = csv.reader(file,
                               delimiter=dialect.delimiter,
                               quotechar=dialect.quotechar)

        # skip header
        csvreader.next()

        for row in csvreader:
            if len(row) != 5:
                print 'Skipping invalid row'
                continue
            car = self.parse_row(row)
            car.save()

    def parse_row(self, row):
        return Car(
            title=row[0],
            link=row[1],
            company=row[2],
            year=int(row[3]),
            engine_type=row[4].lower()[0]
        )
