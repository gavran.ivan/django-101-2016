from django.core.urlresolvers import reverse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from cars.models import Car
from cars.forms import CarEditForm, CarModelForm


class CarListView(LoginRequiredMixin, View):
    """
    View for displaying list of cars.
    """

    def get(self, request, *args, **kwargs):
        context = {}
        cars = Car.objects.all()
        query = request.GET.get('q')
        if query:
            context['query'] = query
            cars = cars.filter(title__contains=query)
        paginator = Paginator(cars, 10)
        page = request.GET.get('page')
        try:
            cars = paginator.page(page)
        except PageNotAnInteger:
            cars = paginator.page(1)
        except EmptyPage:
            cars = paginator.page(paginator.num_pages)

        context['cars'] = cars
        context['new_car_form'] = CarModelForm()

        return render(request, 'cars/car_list.html', context=context)

    def post(self, request, *args, **kwargs):

        car_form = CarModelForm(request.POST)

        if car_form.is_valid():
            car = car_form.save()
            messages.success(request, 'New Car created!')
            return HttpResponseRedirect(reverse('car_list'))
        return render(request, 'cars/car_edit.html', context={
            'new_car_form': car_form
        })


class CarDetailsView(View):
    """
    View for displaying car details.
    """

    def get(self, request, *args, **kwargs):
        return render(
            request, 
            'cars/car_details.html', 
            context={'car': get_object_or_404(Car, id=kwargs['car_id'])})


class CarEditView(View):
    """
    View for editing car details.
    """

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            car = Car.objects.get(id=kwargs['car_id'])
            context['car'] = car
        except Car.DoesNotExist:
            messages.error(request,
                           'No car with id: {}'.format(kwargs['car_id']))

        context['car_form'] = CarModelForm(instance=car)
        return render(request, 'cars/car_edit.html', context=context)

    def post(self, request, *args, **kwargs):

        car = Car.objects.get(id=kwargs['car_id'])
        car_form = CarModelForm(request.POST, instance=car)

        if car_form.is_valid():
            car = car_form.save()
            messages.success(request, 'Object saved!')
            return HttpResponseRedirect(reverse('car_details', kwargs={
                'car_id': car.id
            }))
        return render(request, 'cars/car_edit.html', context={
            'car': car,
            'car_form': car_form
        })


class CarDeleteView(View):
    """
    View for editing car details.
    """

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            car = Car.objects.get(id=kwargs['car_id'])
            context['car'] = car
        except Car.DoesNotExist:
            messages.error(request,
                           'No car with id: {}'.format(kwargs['car_id']))
        return render(request, 'cars/car_delete.html', context=context)

    def post(self, request, *args, **kwargs):
        car = Car.objects.get(id=kwargs['car_id'])
        car.delete()
        messages.success(request, 'Car deleted!')
        return HttpResponseRedirect(reverse('car_list'))
