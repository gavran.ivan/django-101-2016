from django.conf.urls import url

from cars import views

urlpatterns = [
    url(r'^$', views.CarListView.as_view(), name='car_list'),
    url(r'^(?P<car_id>[\w]+)/$', views.CarDetailsView.as_view(), name='car_details'),
    url(r'^(?P<car_id>[\w]+)/edit/$', views.CarEditView.as_view(), name='car_edit'),
    url(r'^(?P<car_id>[\w]+)/delete/$', views.CarDeleteView.as_view(), name='car_delete'),
]
