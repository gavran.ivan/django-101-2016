from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed
from django.views import View


class CalcView(View):
    def get(self, request):
        pass

    def post(self, request):
        return HttpResponseNotAllowed(['get'])


def calc_path(request, op):

    if request.method != 'GET':
        return HttpResponseNotAllowed(['get'])

    x, y = request.GET.get('x'), request.GET.get('y')

    if x is None or y is None:
        return HttpResponseBadRequest('Required query param missing. Required params: x, y, op')

    try:
        x = int(x)
    except ValueError:
        return HttpResponseBadRequest('Invalid value for parameter x. Must be int')
    try:
        y = int(y)
    except ValueError:
        return HttpResponseBadRequest('Invalid value for parameter x. Must be int')

    if op == 'add':
        res = x + y
        op_out = '+'
    elif op == 'sub':
        res = x - y
        op_out = '-'
    elif op == 'div':
        res = x / y
        op_out = '/'
    elif op == 'mul':
        res = x * y
        op_out = '*'
    else:
        return HttpResponseBadRequest('Invalid operation {}. Supported operationsa are [add, sub, div, mul]'.format(op))

    return HttpResponse('{}{}{}={}'.format(x, op_out, y, res))


def calc(request):

    if request.method != 'GET':
        return HttpResponseNotAllowed(['get'])


    x, y, op = request.GET.get('x'), request.GET.get('y'), request.GET.get('op')

    if x is None or y is None or op is None:
        return HttpResponseBadRequest('Required query param missing. Required params: x, y, op')

    try:
        x = int(x)
    except ValueError:
        return HttpResponseBadRequest('Invalid value for parameter x. Must be int')
    try:
        y = int(y)
    except ValueError:
        return HttpResponseBadRequest('Invalid value for parameter x. Must be int')

    if op == 'add':
        res = x + y
        op_out = '+'
    elif op == 'sub':
        res = x - y
        op_out = '-'
    elif op == 'div':
        res = x / y
        op_out = '/'
    elif op == 'mul':
        res = x * y
        op_out = '*'
    else:
        return HttpResponseBadRequest('Invalid operation. Supported operationsa are [add, sub, div, mul]')

    return HttpResponse('{}{}{}={}'.format(x, op_out, y, res))
